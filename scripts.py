import argparse
from subprocess import check_call


def reformat() -> None:
    check_call(["black", "."])


def lint() -> None:
    check_call(
        ["black", "--check", "--diff", "--color", "."],
    )


def start() -> None:
    check_call(["python", "manage.py", "runserver"])


def unit_test() -> None:
    check_call(["python", "manage.py", "test"])


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser_defaults = {"dest": "fns", "action": "append_const"}
    parser.add_argument("--reformat", const=reformat, **parser_defaults)
    parser.add_argument("--lint", const=lint, **parser_defaults)
    parser.add_argument("--start", const=start, **parser_defaults)
    parser.add_argument("--test", const=unit_test, **parser_defaults)
    args = parser.parse_args()
    [fn() for fn in args.fns]
