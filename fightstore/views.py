from django.views import generic

from fightstore.models import Fight


class IndexView(generic.ListView):
    template_name = "fightstore/index.html"
    model = Fight
