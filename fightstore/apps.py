from django.apps import AppConfig


class FightstoreConfig(AppConfig):
    name = "fightstore"
