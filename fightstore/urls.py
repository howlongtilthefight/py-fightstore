from django.urls import path

from . import views

app_name = "fightstore"
urlpatterns = [
    # ex: /fightstore/
    path("", views.IndexView.as_view(), name="index"),
]
