from django.db import models


class Fight(models.Model):
    event_url_path = models.CharField(max_length=255)
    event_name = models.CharField(max_length=255)

    promotion_date = models.DateTimeField()
    promotion_org = models.CharField(max_length=255)

    listing_bout = models.CharField(max_length=255, default=None)
    listing_billing = models.CharField(max_length=255)

    geo_country = models.CharField(max_length=255)
    geo_region = models.CharField(max_length=255)
    geo_sport = models.CharField(max_length=255, null=True)
    geo_venue = models.CharField(max_length=255)
    geo_venue_location = models.CharField(max_length=255)

    timestamp = models.DateTimeField(auto_now_add=True)
