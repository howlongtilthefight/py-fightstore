from pathlib import Path


def load_fixture_from_file(filename: str) -> str:
    path = Path(__file__).parent / f"fixtures/{filename}"
    fixture_data = path.read_text()
    return fixture_data
