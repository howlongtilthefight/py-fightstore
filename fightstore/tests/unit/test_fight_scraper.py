from datetime import timedelta

from dateparser.timezone_parser import StaticTzInfo
from django.test import TestCase
from django.utils import timezone
from freezegun import freeze_time

from ..test_utils import load_fixture_from_file
from ... import scraper


# Override default reference date to match the time fixture data was pulled
@freeze_time("2020-09-12")
class FightScraperTests(TestCase):
    def setUp(self) -> None:
        # Load HTML snapshot
        self.html_fixture = load_fixture_from_file(
            "tapology_fightcenter_major_upcoming.html"
        )

    def test_makes_models_from_html(self):
        fights = scraper.parse_fights(self.html_fixture)
        self.assertEqual(len(fights), 25)

        fight1 = fights[0]
        fight2 = fights[1]

        self.assertEqual(self.make_date(mo=9, d=12, hr=17), fight1.promotion_date)
        self.assertEqual("UFC", fight1.promotion_org)

        self.assertEqual("UFC Fight Night", fight1.event_name)
        self.assertEqual(
            "/fightcenter/events/70698-ufc-fight-night", fight1.event_url_path
        )

        self.assertIsNone(
            fight1.listing_bout
        )  # Fight 1 does not list a "bout" (e.g. "Middleweight Title Fight")
        self.assertEqual(
            "Bantamweight Title Fight", fight2.listing_bout
        )  # so let's check Fight 2
        self.assertEqual("Waterson vs. Hill", fight1.listing_billing)

        self.assertEqual("Us", fight1.geo_country)
        self.assertEqual("US West Region", fight1.geo_region)
        self.assertEqual("MMA", fight1.geo_sport)
        self.assertIsNone(
            fight1.geo_venue
        )  # This one does not list a venue (probably won't until they are reopened)
        self.assertEqual("Las Vegas, NV", fight1.geo_venue_location)

        self.assertIsNone(
            fight1.timestamp, "Timestamp instantiation should be delegated to Model"
        )

    def test_parse_all_dates(self):
        expected_dates = [
            self.make_date(mo=9, d=12, hr=17),
            self.make_date(mo=9, d=12, hr=22),
            self.make_date(mo=9, d=15, hr=20),
            self.make_date(mo=9, d=17, hr=20),
            self.make_date(mo=9, d=18, hr=8, mins=30),
            self.make_date(mo=9, d=19, hr=14),
            self.make_date(mo=9, d=19, hr=17),
            self.make_date(mo=9, d=26, hr=11),
            self.make_date(mo=9, d=26, hr=14),
            self.make_date(mo=9, d=27, hr=3),
            self.make_date(mo=10, d=1, hr=16),
            self.make_date(mo=10, d=3, hr=11),
            self.make_date(mo=10, d=3, hr=17),
            self.make_date(mo=10, d=4, hr=11),
            self.make_date(mo=10, d=10, hr=11, mins=30),
            self.make_date(mo=10, d=10, hr=12),
            self.make_date(mo=10, d=10, hr=18),
            self.make_date(mo=10, d=15, hr=21),
            self.make_date(mo=10, d=17, hr=18),
            self.make_date(mo=10, d=24, hr=22),
            self.make_date(mo=10, d=29, hr=21),
            self.make_date(mo=10, d=31, hr=18),
            self.make_date(mo=11, d=3, hr=20),
            self.make_date(mo=11, d=7, hr=18),
            self.make_date(mo=11, d=10, hr=20),
        ]

        fights = scraper.parse_fights(self.html_fixture)
        actual_dates = [fight.promotion_date for fight in fights]

        self.assertListEqual(expected_dates, actual_dates)

    @staticmethod
    def make_date(
        y=2020,
        mo=9,
        d=12,
        hr=17,
        mins=0,
        tzinfo=StaticTzInfo("ET", timedelta(days=-1, hours=19)),
    ):
        return timezone.datetime(
            year=y, month=mo, day=d, hour=hr, minute=mins, tzinfo=tzinfo
        )
