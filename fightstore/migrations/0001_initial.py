# Generated by Django 3.1.1 on 2020-09-12 21:32

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = []

    operations = [
        migrations.CreateModel(
            name="Fight",
            fields=[
                (
                    "id",
                    models.AutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("event_date", models.DateTimeField()),
                ("event_org", models.CharField(max_length=255)),
                ("event_name", models.CharField(max_length=255)),
                ("event_bout", models.CharField(max_length=255)),
                ("event_billing", models.CharField(max_length=255)),
                ("event_url_path", models.CharField(max_length=255)),
                ("geo_country", models.CharField(max_length=255)),
                ("geo_region", models.CharField(max_length=255)),
                ("geo_venue", models.CharField(max_length=255)),
                ("geo_venue_location", models.CharField(max_length=255)),
                ("timestamp", models.DateTimeField(auto_now=True)),
            ],
        ),
    ]
