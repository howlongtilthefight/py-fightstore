from typing import List, Optional

import bs4
import dateparser
from bs4 import BeautifulSoup

from fightstore.models import Fight


def parse_fights(html: str) -> List[Fight]:
    soup = BeautifulSoup(html, "html5lib")
    listings: List[bs4.Tag] = soup.find_all(attrs={"class": "fcListing"})

    fights: List[Fight] = []

    for listing in listings:
        fight = Fight()

        div_promotion = listing.find("div", attrs={"class", "promotion"})
        div_promotion_logo = listing.find("div", attrs={"class", "promotionLogo"})
        div_geo = listing.find("div", attrs={"class", "geography"})
        div_listing = listing.find("div", attrs={"class", "listing"})

        # Promotion
        date_string = div_promotion.find(attrs={"class": "datetime"}).get_text(
            strip=True
        )
        fight.promotion_date = dateparser.parse(date_string, languages=["en"])
        fight.promotion_org = div_promotion_logo.find(
            "img", attrs={"class": "promotionLogo"}
        ).attrs["alt"]

        # Event
        a_event_url = div_promotion.find("span", attrs={"class", "name"}).find("a")
        fight.event_name = a_event_url.get_text(strip=True)
        fight.event_url_path = a_event_url.attrs["href"]

        # Geo
        img_event_flag = div_geo.find("img", attrs={"class": "eventFlag"})
        fight.geo_country = img_event_flag.attrs["alt"] if img_event_flag else None
        fight.geo_region = select_text_by_class(div_geo, "region")
        fight.geo_sport = select_text_by_class(div_geo, "sport")
        fight.geo_venue = select_text_by_class(div_geo, "venue")
        fight.geo_venue_location = select_text_by_class(div_geo, "venue-location")

        # Listing
        div_listing_bouts = div_listing.find_all(attrs={"class", "bout"})
        fight.listing_billing = select_text_by_class(div_listing, "billing")
        fight.listing_bout = (
            div_listing_bouts[0].get_text(strip=True)
            if len(div_listing_bouts) > 1
            else None
        )

        fights.append(fight)

    return fights


def select_text_by_class(el: bs4.Tag, class_name: str) -> Optional[str]:
    found = el.find(attrs={"class": class_name})
    return found.get_text(strip=True) if found else None
